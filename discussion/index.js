console.log("Goodluck")

// Array Methods - Javascript has built-in functions and methods for arrays. This allows us to manipulate and access array items.


//Mutator Methods
/*
	- mutator methods are functions that "mutate" or change an array after they're created.
	- these methods manipulate the orignal array performing various task such as adding and removing elements.
*/

//push()
/*
	- adds an element in the end of an array and returns the array's length.

	Syntax:
		arrayName.push();
*/

let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];

console.log('Current array:');
console.log(fruits);

fruitsLength = fruits.push('Mango');
console.log(fruitsLength); // will show the new count of array length
console.log('Mutatad array from push method:');
console.log(fruits);

//Adding multiple elements to an array
fruits.push('Avocado', 'Guava', 'lemon', 'banana');
console.log('Mutated Array from push method:');
console.log(fruits);

//==========================

// pop()
/*
	- removes the last element in an array AND returns the removed element.

	Syntax:
		arrayName.pop()
*/


let removedFruit = fruits.pop();
console.log(removedFruit); // will show the removed fruit
console.log('Mutated array from pop method:');
console.log(fruits);

// ===================

// unshift()
/*
	- adds one or more elements at the beginning of an array.

	Syntax:
		arrayName.unshift('elementA');
		arrayName.unshift('elementA', 'element b');
*/

fruits.unshift('lime', 'Saging');
console.log('Mutated array from unshift method:');
console.log(fruits);


// ====================

// shift()
/*
	- removes an element at the beginning of an array and returns the removed element

	Syntax:
		arrayName.shift();

*/

let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log("Mutated array from shift method:");
console.log(fruits);

// =======================

// splice()
/*
	-simultaneously removes elements from a specified index number and adds element

	syntax:
		arrayName.splice(startingIndex, deleteCount, elementsTobeAdded)
*/

/*const try1 = fruits.splice(1, 0, 'lime', 'cherry');
console.log(try1); // used to see what you removed
*/

// fruits.splice(1, 0, 'lime', 'cherry'); use 0 on if you dont want to delete and add element from 1

// fruits.splice(1, 2,); use to delete elements from 1 - 2 only


fruits.splice(1, 2, 'lime', 'cherry');
console.log('Mutated array from splice method:');
console.log(fruits); // replaces the 1 and 2 elements by lime and cherry


//==================
/*
	- rearranged the array elements in alphanumeric order. Upper case prioritize it

	Syntax:
		arrayName.sort();
*/

const randomThings = ["Cat", "boy", "apps", "zoo"]

randomThings.sort();
console.log('Mutated array from sort method: with Constant');
console.log(randomThings);


fruits.sort();
console.log('Mutated array from sort method:');
console.log(fruits);


//==================
// reverse()
/*
	- reversed the current order of the array elements
	- opposite of sort
	- based on array only not revrese alphanumeric order
	Syntax:
		arrayName.reverse();
*/

randomThings.reverse();
console.log('Mutated array from reverse method with constant:');
console.log(randomThings);


fruits.reverse();
console.log('Mutated array from reverse method:');
console.log(fruits);


//=======================

// Non-Mutator Methods
/*
	- Non-Mutator methods are functions that do not modify or change an array after they're created.
	- These methods do not manipulate the original array performing various task such as returning elements from an array and combining array and printing the output.
*/


//===============

// indexOf()
/*
	- returns the index number of the first matching element found in an array
	- if no match was found, the result will be -1 
	- the search process will be done from first element proceeding to the last element

	Syntax:
		arrayName.indexof(searchValue)
*/

let countries = ["US", "PH", "CAN", "SG", "TH", "BR", "FR", "DE"]

console.log(countries);
let firstIndex = countries.indexOf('CAN');
console.log('Result of indexOf method: ' + firstIndex);


//============

//slice()
/*
	- portion/slices elements from an array AND returns a new array.
	- does not affect the original array

	Syntax:
		arrayName.slice(startingIndex);
		arrayName.slice(startingIndex, endingIndex);
*/


// Slicing off elements from a specified index to the first element

let slicedArrayA = countries.slice(4);
console.log("Result from sliceA method:");
console.log(countries); // not affected
console.log(slicedArrayA);
console.log(countries); // not affected


// Slicing off elements from a specified index to another index

// Note: the last element is not included.
let sliceArrayB = countries.slice(1,3);
console.log("Result from sliceB method using target index:");
console.log(sliceArrayB);


// slicing off elements starting from the last element of an array.
let slicedArrayC = countries.slice(-3);
console.log("Result from SliceC method:");
console.log(slicedArrayC);


// ==================

// forEach();
/*
	- similar to a for loop that iterates on each array element.
	- for each item in the array, the anonymous function passed in forEach() method will be run.
	- will print the array vertically
	- arrayName - plural for good practice
	- parameter - singular
	- note: it must have function

	SYNTAX:
			arrayName.forEach(function(indivElement){
				statement
			})
*/


countries.forEach(function(country) {
	console.log(country);
})


// convert to a function
/*countries.forEach(myFunction);

function myFunction(value, index, array) {
	// txt += value + "<br>";
	if(value){
		document.write(value + "<br>");
	}
}
console.log(countries);*/ 



// inlcudes()
/*
	includes()

	- if meron in tagalog
	- includes() method checks if the argument passed can be found in the array.
	- it returns boolean which can be saved in a variable
		- return true if the argument is found in the array.
		- return falase if it is not.

		Syntax:
			arrayName.includes(<argumentToFind>)
*/

let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

let productFound1 = products.includes("Mouse");

console.log(productFound1);//returns true

let productFound2 = products.includes("Headset");

console.log(productFound2);//returns false