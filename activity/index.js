console.log("Fighting!")

/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [
    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];

/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - invoke and register a new user.
        - outside the function log the registeredUsers array.

*/

function register(inputNewUser) {

   isExistingUser = registeredUsers.includes(inputNewUser);

   if (isExistingUser) {
        alert("Registration failed. Username already exists");
        return;
   }
  
    registeredUsers.push(inputNewUser);
    alert("Thank you for registering!");
    /*console.log(registeredUsers)*/
   
}


/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - this function should be able to receive a string.
        - determine if the input username exists in our registeredUsers array.
            - if it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - if it is not, show an alert window with the following message:
                - "User not found."
        - invoke the function and add a registered user in your friendsList.
        - Outside the function log the friendsList array in the console.

*/

function addFriend(newFriend) {
    isExistingUser = registeredUsers.includes(newFriend);
    isExistingFriend = isExistingUser && friendsList.includes(newFriend);
    isNewFriend = !isExistingFriend;

    if (!isExistingUser) {
        alert("User not found.");
        return;
    }

    if (isNewFriend) {
        friendsList.push(newFriend);
        alert("You have added " + newFriend + " as a friend!");
        return;
        }

    if (isExistingFriend) {
        alert("You are friend of this user already!");
        return;
    }

}


/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.

*/

/*
function displayFriends(friendsList)
friendsList.forEach(function(friendsList) {
    console.log(friendsList);
})
*/

function displayFriends() {

    hasNoFriend = friendsList.length === 0
    if (hasNoFriend) {
        alert("You currently have 0 friends. Add one first.")
        return;
    }

    friendsList.forEach(function(friend) {
        console.log(friend);
    })
    

    // ES 6 Solution
   /* else {
        friendsList.forEach(friend => console.log(friend))
    }*/ 
}

/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function

*/

function currentFriendCount() {
    hasFriends = friendsList.length >= 1
    if (hasFriends) {
        
        // console.log("You currently have " + friendsList.length + " friends.");
        console.log(`You currently have ${friendsList.length} friends.`)
        return;
    }

    alert("You currently have 0 friends. Add one first.");
}



/*
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - Outside the function log the friendsList array.

*/


function deleteFriend(friendName) {

    hasNoFriend = friendsList.length === 0
    isfriends = friendsList.includes(friendName);

    if (hasNoFriend) {
        alert("You currently have 0 friends. Add one first.");
        return;
    }

    if (!isfriends) {
        alert(`You have no friend named ${friendName} based on your list`);
        return;
    }

    friendsList = friendsList.filter(function(friend) {
        return friend !== friendName;
    })
      
    //Arrow Function solution
    // friendsList = friendsList.filter(friend => friend !== friendName)
}


/*
    Stretch Goal:

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/

function thankyouMessage() {
alert("Thank you for viewing!");
}






